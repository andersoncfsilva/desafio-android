package br.com.andersoncfsilva.data.repository.datasource

import br.com.andersoncfsilva.data.net.GitService
import br.com.andersoncfsilva.data.repository.datastore.CloudGitDataStore
import br.com.andersoncfsilva.data.util.MockHelper
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CloudGitDataStoreTest {

    val page = "1"
    val owner = "owner"
    val repo = "repo"

    lateinit var cloudGitDataStore: CloudGitDataStore

    @Mock lateinit var mockGitService: GitService

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        cloudGitDataStore = CloudGitDataStore(mockGitService)
        given(mockGitService.repositoryList(page)).willReturn(Observable.just(MockHelper.repositoriesResponse))
        given(mockGitService.pullRequestList(owner, repo)).willReturn(Observable.just(MockHelper.pullResponseList))
    }

    @Test
    fun testGetRepositoryListFromApi(){
        cloudGitDataStore.repositoryList(page).test().assertComplete()
        verify(mockGitService).repositoryList(page)
    }

    @Test
    fun testGetPullRequestListFromApi(){
        cloudGitDataStore.pullRequestList(owner, repo).test().assertComplete()
        verify(mockGitService).pullRequestList(owner, repo)
    }
}