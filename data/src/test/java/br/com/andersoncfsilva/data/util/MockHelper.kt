package br.com.andersoncfsilva.data.util

import br.com.andersoncfsilva.data.net.reponse.PullResponse
import br.com.andersoncfsilva.data.net.reponse.RepositoriesResponse
import br.com.andersoncfsilva.domain.entity.*
import com.google.gson.reflect.TypeToken
import java.util.*


object MockHelper {

    val repositoriesResponse: RepositoriesResponse
    get() {
       return JsonDeserializeUtil.constructUsingGson(RepositoriesResponse::class.java, "repositories.json")
    }

    val pullResponseList: List<PullResponse>
        get() {
            val listType = object : TypeToken<ArrayList<PullResponse>>() {}.type

            return JsonDeserializeUtil.constructUsingGson(listType, "pulls.json")
        }

    val gitRepositoryList : GitRepositoryList
    get() {
        val totalItems = 1
        val owner = Owner(name = "name", photo = "http://www.photo.com.br")
        val repo =  GitRepository(
                name = "name",
                description =  "description",
                starsCount = 2,
                forksCount = 3,
                owner = owner
        )
        return GitRepositoryList(totalItems = totalItems, repositories = arrayListOf(repo))
    }

    val gitPullList : GitPullList
        get() {
            val openedCount = 1
            val closedCount = 1
            val owner = Owner(name = "name", photo = "http://www.photo.com.br")
            val pull = GitPull(
                    title = "title",
                    body = "body",
                    owner = owner,
                    created = Date()
            )
            val pulls = arrayListOf(pull)
            return GitPullList(openedCount = openedCount, closedCount = closedCount, pulls = pulls)
        }

}