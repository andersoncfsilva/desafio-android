package br.com.andersoncfsilva.data.mapper

import br.com.andersoncfsilva.data.util.MockHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MapperTest {


    @Test
    fun TestMapperToGitRepositoryList(){
        val response = MockHelper.repositoriesResponse
        val repositories = response.toGitRepositoryList()

        checkNotNull(repositories)
        assert(response.totalCount == repositories.totalItems)

        repositories.repositories.forEachIndexed { index, (name, description, forksCount, starsCount, owner) ->
            val item = response.items!![index]!!
            assert(description == item.description)
            assert(forksCount == item.forksCount)
            assert(starsCount == item.stargazersCount)
            assert(name == item.name)
            val ownerResponse = item.owner!!
            assert(owner.name == ownerResponse.login)
            assert(owner.photo == ownerResponse.avatarUrl)
        }
    }

    @Test
    fun TestMapperToGitPullList(){
        val response = MockHelper.pullResponseList
        val pulls = response.toGitPullList()

        checkNotNull(pulls)
        assert(response.count() == response.count())

        val opened = response.count { it.closedAt == null }
        val closed = response.count { it.closedAt != null }
        assert(pulls.openedCount == opened)
        assert(pulls.closedCount == closed)

        pulls.pulls.forEachIndexed { index, (title, body, created, owner) ->
            val item = response[index]
            assert(title == item.title)
            assert(body == item.body)
            assert(created == item.createdAt)
            val ownerResponse = item.user!!
            assert(owner.name == ownerResponse.login)
            assert(owner.photo == ownerResponse.avatarUrl)
        }

    }

}
