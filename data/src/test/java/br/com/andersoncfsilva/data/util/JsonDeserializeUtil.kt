package br.com.andersoncfsilva.data.util

import com.google.gson.GsonBuilder
import java.io.File
import java.lang.reflect.Type


object JsonDeserializeUtil {

    fun <T> constructUsingGson(type: Type, fileName: String): T {
        val classLoader = this.javaClass.classLoader
        val resource = classLoader.getResource(fileName)
        val file = File(resource.path)
        val json = file.readText()

        val gsonBuilder = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")

        return gsonBuilder.create().fromJson(json, type)
    }
}