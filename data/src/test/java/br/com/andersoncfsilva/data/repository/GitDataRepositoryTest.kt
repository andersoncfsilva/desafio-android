package br.com.andersoncfsilva.data.repository


import br.com.andersoncfsilva.data.repository.datastore.GitDataStore
import br.com.andersoncfsilva.data.util.MockHelper
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GitDataRepositoryTest {

    val page = "1"
    val owner = "owner"
    val repo = "repo"

    lateinit var dataRepository: GitDataRepositoryImpl

    @Mock lateinit var mockGitDataStore: GitDataStore


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataRepository = GitDataRepositoryImpl(mockGitDataStore)
        given(mockGitDataStore.repositoryList(page)).willReturn(Observable.just(MockHelper.gitRepositoryList))
        given(mockGitDataStore.pullRequestList(owner, repo)).willReturn(Observable.just(MockHelper.gitPullList))
    }

    @Test
    fun testGetRepositoryListHappyCase() {
        dataRepository.repositories(page).test().assertComplete()
        verify(mockGitDataStore).repositoryList(page)
    }

    @Test
    fun testGetPullRequestListHappyCase() {
        dataRepository.pullRequests(owner, repo).test().assertComplete()
        verify(mockGitDataStore).pullRequestList(owner, repo)
    }
}
