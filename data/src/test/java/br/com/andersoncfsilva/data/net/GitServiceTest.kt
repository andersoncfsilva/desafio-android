package br.com.andersoncfsilva.data.net

import br.com.andersoncfsilva.data.exception.NetworkConnectionException
import br.com.andersoncfsilva.data.util.MockHelper
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GitServiceTest{

    val page = "1"
    val owner = "owner"
    val repo = "repo"

    lateinit var service: GitServiceImpl

    @Mock lateinit var mockApi: RestApi

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        service = GitServiceImpl(mockApi)
    }

    @Test
    fun testGetRepositoryListFromRetrofit(){
        given(mockApi.getRepositories(page)).willReturn(Observable.just(MockHelper.repositoriesResponse))
        service.repositoryList(page).test().assertComplete()
        verify(mockApi).getRepositories(page)
    }

    @Test
    fun testGetPullRequestListFromRetrofit(){
        given(mockApi.getPulls(owner, repo)).willReturn(Observable.just(MockHelper.pullResponseList))
        service.pullRequestList(owner, repo).test().assertComplete()
        verify(mockApi).getPulls(owner, repo)
    }

    @Test
    fun testGetRepositoryListWithError(){
        val exception = RuntimeException("mock error")
        given(mockApi.getRepositories(page)).willReturn(Observable.error(exception))
        service.repositoryList(page).test().onError(NetworkConnectionException(exception))
        verify(mockApi).getRepositories(page)
    }

    @Test
    fun testGetPullRequestListWithError(){
        val exception = RuntimeException("mock error")
        given(mockApi.getPulls(owner, repo)).willReturn(Observable.error(exception))
        service.pullRequestList(owner, repo).test().onError(NetworkConnectionException(exception))
        verify(mockApi).getPulls(owner, repo)
    }



}