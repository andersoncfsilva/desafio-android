package br.com.andersoncfsilva.data.net

import br.com.andersoncfsilva.data.net.reponse.PullResponse
import br.com.andersoncfsilva.data.net.reponse.RepositoriesResponse
import br.com.andersoncfsilva.data.exception.NetworkConnectionException
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GitServiceImpl @Inject constructor(val restApi: RestApi) : GitService {

    override fun repositoryList(page: String): Observable<RepositoriesResponse> {
        return restApi.getRepositories(page)
                .doOnError { throw NetworkConnectionException(it.cause) }
    }

    override fun pullRequestList(owner: String, repo: String): Observable<List<PullResponse>> {
        return restApi.getPulls(owner, repo)
                .doOnError { throw NetworkConnectionException(it.cause) }
    }
}