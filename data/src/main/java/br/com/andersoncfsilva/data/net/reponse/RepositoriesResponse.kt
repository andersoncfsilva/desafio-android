package br.com.andersoncfsilva.data.net.reponse

import com.google.gson.annotations.SerializedName

data class RepositoriesResponse(

	@field:SerializedName("total_count")
	val totalCount: Int? = null,

	@field:SerializedName("incomplete_results")
	val incompleteResults: Boolean? = null,

	@field:SerializedName("items")
	val items: List<ItemsItem?>? = null
)