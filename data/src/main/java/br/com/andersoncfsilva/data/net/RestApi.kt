package br.com.andersoncfsilva.data.net

import br.com.andersoncfsilva.data.net.reponse.PullResponse
import br.com.andersoncfsilva.data.net.reponse.RepositoriesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface RestApi {

    @GET("/search/repositories?q=language:Java&sort=stars&page={page}")
    fun getRepositories(@Path("page") page: String): Observable<RepositoriesResponse>

    @GET("/repos/{owner}/{repo}/pulls")
    fun getPulls(@Path("owner") owner: String, @Path("repo") repo: String): Observable<List<PullResponse>>

}