package br.com.andersoncfsilva.data.net

import br.com.andersoncfsilva.data.net.reponse.PullResponse
import br.com.andersoncfsilva.data.net.reponse.RepositoriesResponse
import io.reactivex.Observable

interface GitService {

    fun repositoryList(page: String): Observable<RepositoriesResponse>
    fun pullRequestList(owner: String, repo: String): Observable<List<PullResponse>>

}