package br.com.andersoncfsilva.data.mapper

import br.com.andersoncfsilva.data.net.reponse.PullResponse
import br.com.andersoncfsilva.data.net.reponse.RepositoriesResponse
import br.com.andersoncfsilva.domain.entity.*

fun RepositoriesResponse.toGitRepositoryList(): GitRepositoryList {

    val totalItems = this.totalCount ?: 0
    val repositories = this.items?.map {
        val owner = Owner(name = it?.owner?.login!!, photo = it.owner.avatarUrl!!)
        GitRepository(
                name = it.name!!,
                description = it.description!!,
                starsCount = it.stargazersCount ?: 0,
                forksCount = it.forksCount ?: 0,
                owner = owner
                )
    } ?: arrayListOf()

    return GitRepositoryList(totalItems = totalItems, repositories = repositories)
}

fun List<PullResponse>.toGitPullList(): GitPullList {
    val openedCount = this.count { it.closedAt == null }
    val closedCount = this.count { it.closedAt != null }
    val pulls = this.map {
        val owner = Owner(name = it.user?.login!!, photo = it.user.avatarUrl!!)
        GitPull(
                title = it.title!!,
                body = it.body!!,
                owner = owner,
                created = it.createdAt!!
        )
    }
    return GitPullList(openedCount = openedCount, closedCount = closedCount, pulls = pulls)
}