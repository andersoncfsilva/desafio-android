package br.com.andersoncfsilva.data.repository.datastore

import br.com.andersoncfsilva.data.mapper.toGitPullList
import br.com.andersoncfsilva.data.mapper.toGitRepositoryList
import br.com.andersoncfsilva.data.net.GitService
import br.com.andersoncfsilva.domain.entity.GitPullList
import br.com.andersoncfsilva.domain.entity.GitRepositoryList
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class CloudGitDataStore @Inject constructor(val gitService: GitService) : GitDataStore {

    override fun repositoryList(page: String): Observable<GitRepositoryList> {
        return gitService.repositoryList(page)
                .map { it.toGitRepositoryList() }
    }

    override fun pullRequestList(owner: String, repo: String): Observable<GitPullList> {
        return gitService.pullRequestList(owner, repo)
                .map { it.toGitPullList() }
    }
}