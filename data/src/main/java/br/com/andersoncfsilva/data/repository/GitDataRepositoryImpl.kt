package br.com.andersoncfsilva.data.repository

import br.com.andersoncfsilva.data.repository.datastore.GitDataStore
import br.com.andersoncfsilva.domain.entity.GitPullList
import br.com.andersoncfsilva.domain.entity.GitRepositoryList
import br.com.andersoncfsilva.domain.repository.GitDataRepository
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class GitDataRepositoryImpl @Inject constructor(val dataStore: GitDataStore) : GitDataRepository {

    fun repositories(page: String): Observable<GitRepositoryList> {
        return dataStore.repositoryList(page)
    }

    fun pullRequests(owner: String, repository: String): Observable<GitPullList> {
        return dataStore.pullRequestList(owner, repository)
    }
}