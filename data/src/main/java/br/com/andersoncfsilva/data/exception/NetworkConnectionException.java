package br.com.andersoncfsilva.data.exception;


public class NetworkConnectionException extends Exception {

  public NetworkConnectionException(final Throwable cause) {
    super(cause);
  }
}
