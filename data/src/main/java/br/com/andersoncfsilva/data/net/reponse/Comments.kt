package br.com.andersoncfsilva.data.net.reponse


import com.google.gson.annotations.SerializedName


data class Comments(

	@field:SerializedName("href")
	val href: String? = null
)