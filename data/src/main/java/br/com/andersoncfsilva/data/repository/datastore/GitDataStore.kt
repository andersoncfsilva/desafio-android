package br.com.andersoncfsilva.data.repository.datastore

import br.com.andersoncfsilva.domain.entity.GitPullList
import br.com.andersoncfsilva.domain.entity.GitRepositoryList
import io.reactivex.Observable

interface GitDataStore{

    fun repositoryList(page: String): Observable<GitRepositoryList>
    fun pullRequestList(owner: String, repo: String): Observable<GitPullList>

}