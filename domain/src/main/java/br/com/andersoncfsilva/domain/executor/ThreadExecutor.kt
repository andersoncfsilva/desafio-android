package br.com.andersoncfsilva.domain.executor

import java.util.concurrent.Executor


interface ThreadExecutor : Executor
