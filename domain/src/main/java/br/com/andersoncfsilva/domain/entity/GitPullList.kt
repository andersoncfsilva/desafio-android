package br.com.andersoncfsilva.domain.entity

import java.util.*


data class GitPullList(val openedCount: Int,
                       val closedCount: Int,
                       val pulls: List<GitPull>)

data class GitPull(
        val title: String,
        val body: String,
        val created: Date,
        val owner: Owner
)