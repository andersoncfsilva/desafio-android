package br.com.andersoncfsilva.domain.entity


data class GitRepositoryList(val totalItems: Int,
                             val repositories: List<GitRepository>)

data class GitRepository(
        val name: String,
        val description: String,
        val forksCount: Int,
        val starsCount: Int,
        val owner: Owner
        )

data class Owner(val name: String,
                 val photo: String)
