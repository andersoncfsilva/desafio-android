package br.com.andersoncfsilva.domain.executor

import io.reactivex.Scheduler


interface PostExecutionThread {
    val scheduler: Scheduler
}
