package br.com.andersoncfsilva.githubapi;

import android.app.Application;

import br.com.andersoncfsilva.githubapi.di.components.ApplicationComponent;
import br.com.andersoncfsilva.githubapi.di.components.DaggerApplicationComponent;
import br.com.andersoncfsilva.githubapi.di.modules.ApplicationModule;


/**
 * Android Main Application
 */
public class AndroidApplication extends Application {

  private ApplicationComponent applicationComponent;

  @Override
  public void onCreate() {
    super.onCreate();
    this.initializeInjector();
  }

  private void initializeInjector() {
    this.applicationComponent = DaggerApplicationComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .build();
  }

  public ApplicationComponent getApplicationComponent() {
    return this.applicationComponent;
  }


}
