package br.com.andersoncfsilva.githubapi.di.modules

import android.content.Context
import br.com.andersoncfsilva.data.executor.JobExecutor
import br.com.andersoncfsilva.data.net.GitService
import br.com.andersoncfsilva.data.net.GitServiceImpl
import br.com.andersoncfsilva.data.net.RestApi
import br.com.andersoncfsilva.data.repository.GitDataRepositoryImpl
import br.com.andersoncfsilva.data.repository.datastore.CloudGitDataStore
import br.com.andersoncfsilva.data.repository.datastore.GitDataStore
import br.com.andersoncfsilva.domain.executor.PostExecutionThread
import br.com.andersoncfsilva.domain.executor.ThreadExecutor
import br.com.andersoncfsilva.domain.repository.GitDataRepository
import br.com.andersoncfsilva.githubapi.AndroidApplication
import br.com.andersoncfsilva.githubapi.executor.UIThread
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    internal fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @Singleton
    internal fun provideGitDataRepository(repo: GitDataRepositoryImpl): GitDataRepository {
        return repo
    }

    @Provides
    @Singleton
    internal fun provideGitDataStore(store: CloudGitDataStore): GitDataStore{
        return store
    }

    @Provides
    @Singleton
    internal fun provideGitService(service: GitServiceImpl): GitService{
        return service
    }

    @Provides @Singleton fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
        return gsonBuilder.create()
    }

    @Provides @Singleton fun provideOkhttpClient(context: Context): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val timeout: Long = 30
        val client = OkHttpClient.Builder()
        val cache = Cache(File(context.cacheDir, "http-cache"), 10 * 1024 * 1024)

        client.networkInterceptors().add(logging)
        client.addNetworkInterceptor { chain ->
            val response = chain.proceed(chain.request())
            val minutes = 5
            val cacheControl = CacheControl.Builder()
                    .maxAge(minutes, TimeUnit.MINUTES)
                    .build()

            response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()
        }
        client.cache(cache)
        client.connectTimeout(timeout, TimeUnit.SECONDS)
        client.writeTimeout(timeout, TimeUnit.SECONDS)
        client.readTimeout(timeout, TimeUnit.SECONDS)

        return client.build()
    }


    @Provides @Singleton fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://api.github.com")
                .client(okHttpClient).build()
    }

    @Provides @Singleton fun provideRestAPI(retrofit: Retrofit): RestApi {
        return retrofit.create(RestApi::class.java)
    }

}
