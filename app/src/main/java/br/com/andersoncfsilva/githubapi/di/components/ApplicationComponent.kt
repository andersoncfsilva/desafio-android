package br.com.andersoncfsilva.githubapi.di.components

import android.content.Context


import javax.inject.Singleton

import br.com.andersoncfsilva.domain.executor.PostExecutionThread
import br.com.andersoncfsilva.domain.executor.ThreadExecutor
import br.com.andersoncfsilva.domain.repository.GitDataRepository
import br.com.andersoncfsilva.githubapi.di.modules.ApplicationModule
import dagger.Component

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    //Exposed to sub-graphs.
    fun context(): Context

    fun threadExecutor(): ThreadExecutor
    fun postExecutionThread(): PostExecutionThread
    fun gitDataRepository(): GitDataRepository
}
